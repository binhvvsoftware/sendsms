<?php
require_once realpath(__DIR__) . '/TechAPI/bootstrap.php';

use TechAPI\Exception;
use TechAPI\Auth\AccessToken;
use TechAPI\Api\SendBrandnameOtp;

try
{
	// get post data
	$arrData = array(
		'Phone'      => '0981036486',
		'BrandName'  => 'DAIHOCFPT',
		'Message'    => 'Test gui tin nhan Brandname'
	);
	
	// call api
	$oGrantType = getTechAuthorization();
	$apiSendBrandname = new SendBrandnameOtp($arrData);
	$arrData = $oGrantType->execute($apiSendBrandname);

	if (! empty($arrData['error']))
	{
		// clear access token when error
		AccessToken::getInstance()->clear();
		
		throw new Exception($arrData['error_description'], $arrData['error']);
	}
	
	// Gửi thành công
	echo '<pre>';
	print_r($arrData);
}
catch (\Exception $ex)
{
	// gửi thất bại
	echo "Mã lỗi: " . $ex->getCode();
	echo "<br>Mô tả: " . $ex->getMessage();
}